import fs from 'fs'
import path from 'path'
import * as U from './utils';

async function getImagesAndArticles() {
    const cwd = process.cwd();
    const articlesFile = path.join(cwd, 'public', 'data.json')

    const { data } = await U.fetchArticleList();

    const promises = data.map(U.fetchArticle)
    const resp = await Promise.all(promises)
    fs.writeFileSync(articlesFile, `${JSON.stringify(resp)}`)
}

getImagesAndArticles();