export interface Resource {
    asset_id: string
    public_id: string
    format: string
    version: number
    resource_type: string
    type: string
    created_at: string
    bytes: number
    width: number
    height: number
    url: string
    secure_url: string
}

export interface Search {
    resources: Resource[]
}

export interface Article {
    id: string
    title: string
    content: string
    image: {
        url: string
    }
    category: {
        id: string
        name: string
    }
    published_at: string
}

export interface ArticleQuery {
    data: Article
}

export interface ArticleQueryList {
    data: Article[]
}