import fetch from 'node-fetch';
import * as M from '../models'

export const fetchArticle = async ({ id }: M.Article): Promise<M.ArticleQuery> => {
    return (await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/articles/${id}?populate=*`)).json()
}

export const fetchArticleList = async (): Promise<M.ArticleQueryList> => {
    return (await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/articles?populate=*`)).json()
}