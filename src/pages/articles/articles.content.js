export default {
    header: {
        title: 'Nieuwsbrieven & Updates',
        subTitle: 'Blijf op de hoogte van de laatste ontwikkelingen',
        background: 'fam_knol_header.jpg'
    }
}