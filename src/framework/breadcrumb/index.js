import React from "react";
import { Grid, Breadcrumbs, Typography, Link } from "@material-ui/core";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import useStyles from './breadcrumb.styles';
import withRoot from "../../theme/withRoot";

function Breadcrumb({ backgroundColor, current }) {
  const classes = useStyles();

  const last = (pathNames, index) => {
    return index === pathNames.length - 1;
  }

  return (
    <Grid
      container
      className={classes.container}
      justify="space-around"
      style={{ backgroundColor }}
    >
      <Grid item xs={12} className={classes.content}>
        {(({ location }) => {
          const pathnames = location.pathname.split("/").filter((x) => x);

          return (
            <Breadcrumbs
              aria-label="breadcrumb"
              separator={<NavigateNextIcon fontSize="small" />}
            >
              <Link color="inherit" href="/">
                Home
              </Link>
              {pathnames.map((_, index) => {
                const to = `/${pathnames.slice(0, index + 1).join("/")}`;
                const isLast = last(pathnames, index);

                return isLast ? (
                  <Typography color="textPrimary" key={to}>
                    {current ? current : to.split("/")?.[1]}
                  </Typography>
                ) : (
                  <Link color="inherit" href={to} key={to}>
                    {to.split("/")?.[1]}
                  </Link>
                );
              })}
            </Breadcrumbs>
          );
        })(window)}
      </Grid>
    </Grid>
  );
}

export default withRoot(Breadcrumb)