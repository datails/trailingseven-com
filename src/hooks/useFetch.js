import { useEffect, useReducer, useRef } from 'react'

const createInitialState = () => ({
  error: undefined,
  data: undefined,
})

const fetchReducer = (state, action) => {
  switch (action.type) {
    case 'loading':
      return { ...createInitialState() }
    case 'fetched':
      return { ...createInitialState(), data: action.payload }
    case 'error':
      return { ...createInitialState(), error: action.payload }
    default:
      return state
  }
}

export function useFetch(url, options) {
  const cache = useRef({})

  // Used to prevent state update if the component is unmounted
  const cancelRequest = useRef(false)

  const [state, dispatch] = useReducer(fetchReducer, createInitialState())

  useEffect(() => {
    if (!url) return

    cancelRequest.current = false

    const fetchData = async () => {
      dispatch({ type: 'loading' })

      if (cache.current[url]) {
        dispatch({ type: 'fetched', payload: cache.current[url] })
        return
      }

      try {
        const response = await fetch(url, options)
        if (!response.ok) {
          throw new Error(response.statusText)
        }

        const data = (await response.json())
        cache.current[url] = data
        if (cancelRequest.current) return

        dispatch({ type: 'fetched', payload: data })
      } catch (error) {
        if (cancelRequest.current) return

        dispatch({ type: 'error', payload: error })
      }
    }

    void fetchData()

    return () => {
      cancelRequest.current = true
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [url])

  return state
}